export class Denuncia{
  public imagem: string;
  public descricao: string;
  public data: Date;
  public categoria: string;
  public localizacao: Geolocation;
  public uid: string;
  public usuario: string;

  constructor (descricao: string, data: Date, categoria:string, usuario: string, localizacao: Geolocation, uid: string, imagem?: string){
      this.descricao = descricao;
      this.data = data;
      this.categoria = categoria;
      this.localizacao = localizacao;
      this.imagem = imagem;
      this.uid = uid;
      this.usuario = usuario;
  }
}
