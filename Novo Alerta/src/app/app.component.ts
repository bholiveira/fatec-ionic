import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';

import { SplashScreen } from '@ionic-native/splash-screen';
import { timer } from 'rxjs/observable/timer';


import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { AngularFireAuth } from 'angularfire2/auth';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any;

  showSplash;
  constructor(platform: Platform, afAuth: AngularFireAuth,splashScreen : SplashScreen) {
    platform.ready().then(() => {
      
      splashScreen.hide();
      
        afAuth.authState.subscribe((state) => {
          if(state == null){
            this.showSplash = true;
            timer(3000).subscribe(() => this.showSplash = false)
            this.rootPage = LoginPage;
          }
          else{
              this.rootPage = HomePage;
          }
        });
    });
  }
}
